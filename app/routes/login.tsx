import {
  Box,
  Button,
  Checkbox,
  Divider,
  FormControl,
  FormErrorMessage,
  HStack,
  Input,
  Text,
  VStack,
} from "@chakra-ui/react";
import type { ActionArgs, LoaderArgs, MetaFunction } from "@remix-run/node";
import { json, redirect } from "@remix-run/node";
import { Form, Link, useActionData } from "@remix-run/react";

import { verifyLogin } from "~/models/user.server";
import { createUserSession, getUserId } from "~/session.server";
import { safeRedirect, validateEmail } from "~/utils";

export async function loader({ request }: LoaderArgs) {
  const userId = await getUserId(request);
  if (userId) return redirect("/");
  return json({});
}

export async function action({ request }: ActionArgs) {
  const formData = await request.formData();
  const email = formData.get("email");
  const password = formData.get("password");
  const redirectTo = safeRedirect(formData.get("redirectTo"), "/");
  const remember = formData.get("remember");

  if (!validateEmail(email)) {
    return json(
      { errors: { email: "Email is invalid", password: null } },
      { status: 400 }
    );
  }

  if (typeof password !== "string" || password.length === 0) {
    return json(
      { errors: { password: "Password is required", email: null } },
      { status: 400 }
    );
  }

  if (password.length < 8) {
    return json(
      { errors: { password: "Password is too short", email: null } },
      { status: 400 }
    );
  }

  const user = await verifyLogin(email, password);

  if (!user) {
    return json(
      { errors: { email: "Invalid email or password", password: null } },
      { status: 400 }
    );
  }

  return createUserSession({
    request,
    userId: user.id,
    remember: remember === "on" ? true : false,
    redirectTo,
  });
}

export const meta: MetaFunction = () => {
  return {
    title: "Login",
  };
};

export default function LoginPage() {
  const actionData = useActionData<typeof action>();

  return (
    <Box
      w="full"
      minH="100vh"
      bg="purple.100"
      display="flex"
      alignItems="center"
      justifyContent="center"
    >
      <Box
        w={["full", "80%", "70%", "50%", "25%"]}
        borderRadius="lg"
        bg="white"
        shadow="lg"
        alignItems="center"
        minH="600px"
        p="4"
      >
        <Text fontSize="4xl" textAlign="center" mb="8">
          Inicia sesión
        </Text>
        <Form noValidate method="post">
          <VStack spacing="4">
            <FormControl isInvalid={Boolean(actionData?.errors?.email)}>
              <Input
                id="email"
                name="email"
                placeholder="Email"
                type="email"
                aria-invalid={actionData?.errors?.email ? true : undefined}
              />
              {actionData?.errors?.email ? (
                <FormErrorMessage>{actionData?.errors?.email}</FormErrorMessage>
              ) : null}
            </FormControl>
            <FormControl isInvalid={Boolean(actionData?.errors?.password)}>
              <Input
                id="password"
                name="password"
                placeholder="Contraseña"
                type="password"
                aria-invalid={actionData?.errors?.password ? true : undefined}
              />
              {actionData?.errors?.password ? (
                <FormErrorMessage>
                  {actionData?.errors?.password}
                </FormErrorMessage>
              ) : null}
            </FormControl>
            <FormControl>
              <Checkbox colorScheme="purple" name="remember">
                Mantener sesión
              </Checkbox>
            </FormControl>
            <Button type="submit" colorScheme="purple" w="full">
              Entrar
            </Button>
            <HStack w="full" my="4">
              <Divider />
              <Text>ó</Text>
              <Divider />
            </HStack>
            <HStack>
              <Text>¿No tienes cuenta?</Text>{" "}
              <Button
                colorScheme="purple"
                variant="link"
                as={Link}
                to="/register"
              >
                Registrate
              </Button>
            </HStack>
          </VStack>
        </Form>
      </Box>
    </Box>
  );
}
