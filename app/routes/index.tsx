import { Box, Button, Divider, HStack, Text } from "@chakra-ui/react";
import { Link } from "@remix-run/react";

export default function Index() {
  return (
    <Box
      w="full"
      minH="100vh"
      bg="purple.100"
      display="flex"
      alignItems="center"
      justifyContent="center"
    >
      <Box
        w={["full", "80%", "70%", "50%", "25%"]}
        borderRadius="lg"
        bg="white"
        shadow="lg"
        alignItems="center"
        minH="600px"
        p="4"
      >
        <Text fontSize="4xl" textAlign="center" mb="8">
          Book it!
        </Text>
        <Box>
          <Button as={Link} w="full" colorScheme="purple" to="/login">
            Inicia sesión
          </Button>
          <HStack my="4">
            <Divider />
            <Text>ó</Text>
            <Divider />
          </HStack>
          <Button as={Link} w="full" colorScheme="purple" to="/register">
            Registrate
          </Button>
        </Box>
      </Box>
    </Box>
  );
}
